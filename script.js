/**
 * 
 */

'use strict';

var QueryObject = function(){
    // hash for count numeric index
    var arrayParamsCounter = {};
    
    /**
     * TODO multi level arrays
     */
    var ARRAY_PATTERN = /\[(.*)\]$/;
    
    Object.defineProperty(this, "pushParam", {
    	value: function(key, value){
	        var param = this[key] || {};
	        var param_key = key;
	        var param_value = value;
	        
	        // check if param is array (key[]=)
	        var multi_key_index = key.match(ARRAY_PATTERN);
	        if(multi_key_index){
	            var array_index = multi_key_index[1];
	            param_key = key.replace(multi_key_index[0], '');
	            param = this[param_key] || {};
	            
	            // need auto increment
	            if(array_index === ''){
	            	array_index = getAI(param_key);
//	            	console.log('hi');
	            // index is specified and number
	            }else if(/[0-9]/.test(array_index)){
	            	
	                setAI(param_key, array_index);	
	            }
	            
	            param[array_index] = param_value;
	            
	        }else{
	        	param = param_value;
	        }
	         param_key, {
	        	value: param
	        };
	        
	        this[param_key] = param;
    	}
    });
    
    function getAI(key){
        var ai = 0;
        if(arrayParamsCounter[key] >= 0){
            ai = ++arrayParamsCounter[key];
        }else{
            ai = arrayParamsCounter[key] = 0;
        }
        return ai;
    }
    
    function setAI(key, value){
    	
    	var currentAI = arrayParamsCounter[key] || 0;
    	if(value >= currentAI){
    		arrayParamsCounter[key] = value;
    	}    	 	
    }
};

var QueryParser = function(config){
    var config = config || {};
    var ARG_SEPARATOR = config.ARG_SEPARATOR || '&';
    var VAL_SEPARATOR = config.VAL_SEPARATOR || '=';
    
    this.parse = function(query){
        var params = new QueryObject();
        var pairs = query.split(ARG_SEPARATOR);
        for(var i = 0; i < pairs.length; i++){
            // pair key => value
            var chanks = pairs[i].split(VAL_SEPARATOR);        
            params.pushParam(chanks[0], chanks[1]);
        }
        
        return params;
    };

    this.serialize = function(params){
    	var queryString = '';
    	for(var param in params){
    		queryString += buildParam(param, params[param]);
    	}
    	return queryString;
    };
    
    function buildParam(key, value){
    	var stringParam = '';
//    	console.log(key, value);
    	if(typeof(value) == 'object'){
    		for(var param in value){
    			stringParam += ARG_SEPARATOR + key + '['+ param +']' + VAL_SEPARATOR + value[param]; 
    		}
    	}else{
    		stringParam = ARG_SEPARATOR + key + VAL_SEPARATOR + value;
    	}
    	return stringParam;
    	
    }
};